#!/bin/bash
set -ex

# Install OS packages
yum -y install epel-release yum-plugin-copr
yum -y copr enable @git-maint/git
yum -y shell /tmp/yum-transaction.txt
yum-builddep -y kernel
yum clean all
rm -rf /var/cache/yum
rm -fv /tmp/yum-transaction.txt
